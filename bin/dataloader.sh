#!/bin/bash

usage() {
	echo "Usage: "
	echo "    dataloader.sh <jar> <config_dir> <login_props> <process_name>"
}

if test "$#" -ne 4; then
	usage
	exit 1
fi

#TODO Set default values, for now take everything from cmd

DATALOADER_JAR=$1
CONFIG_DIR=$2
LOGIN_PROPS=$3
PROCESS_NAME=$4

CMD="java -cp $DATALOADER_JAR -Dsalesforce.config.dir=$CONFIG_DIR -Dlogin.property.path=$LOGIN_PROPS com.salesforce.dataloader.process.ProcessRunner process.name=$PROCESS_NAME"

# uncomment to debug
#echo $CMD

$CMD
